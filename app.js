const express = require('express');
const body_parser = require('body-parser');
const path = require('path');
const mongoose = require('mongoose')

Users = require('./models/users');
Products = require('./models/products');


var app = express();

// Connect to mongoose
mongoose.connect('mongodb://localhost/newnotedacademy');
var db = mongoose.connection;

app.listen(8200, function() {
    console.log('Server started on Port 8200...')
})
// app.set('port', 8081);

// View engine
app.set('view engine', 'ejs');
app.set('views', path.join(__dirname, 'views'));

// Body Parser Middleware
app.use(body_parser.json());
app.use(body_parser.urlencoded({extended: false}));

// Static path
app.use(express.static(path.join(__dirname, 'static')));

app.use(function(req, res, next) {
    res.setHeader("Access-Control-Allow-Origin", "*");
    res.setHeader('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, PATCH, DELETE');
    res.setHeader("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
    next();
  });


app.get('/', function(req, res){
    res.render('storefront/index');
});

app.get('/checkout', function(req, res){
    res.render('storefront/checkout');
});

app.get('/product', function(req, res){
    res.render('storefront/product');
});

app.get('/store', function(req, res){
    res.render('storefront/store');
});

app.get('/blank', function(req, res){
    res.render('storefront/blank');
});

app.get('/admin', function(req, res){
    res.render('admin/index');
});

app.get('/admin/add-product', function(req, res){
    res.render('admin/add-product');
});

app.get('/admin/edit-product', function(req, res){
    res.render('admin/edit-product');
});

app.get('/admin/forgot-password', function(req, res){
    res.render('admin/forgot-password');
});

app.get('/admin/inventory', function(req, res){
    res.render('admin/inventory');
});

app.get('/admin/login', function(req, res){
    res.render('admin/login');
});

app.get('/admin/records', function(req, res){
    res.render('admin/records');
});

app.get('/admin/register', function(req, res){
    res.render('admin/register');
});

app.get('/admin/site', function(req, res){
    res.render('admin/site');
});

app.get('/admin/users', function(req, res){
    res.render('admin/users');
});

app.get('/api/users', function(req, res){
    Users.getUserInfo(function(err, users){
        if(err){
            throw err;
        }
        res.send(users);
    })
});

// catch 404 and forward to error handler
// app.use(function(req, res, next) {
//     var err = new Error('Not Found');
//     err.status = 404;
//     next(err);
// });
  
function randomValueHex(length){
    return crypto.randomBytes(Math.ceil(length/2)).toString('hex').slice(0,length);
}

// // error handlers

// // development error handler
// // will print stacktrace
// if (app.get('env') === 'development') {
//     app.use(function(err, req, res, next) {
//         res.status(err.status || 500);
//         res.render('error', {
//         message: err.message,
//         error: err
//         });
//     });
// }

// // production error handler
// // no stacktraces leaked to user
// app.use(function(err, req, res, next) {
//     res.status(err.status || 500);
//     res.render('error', {
//         message: err.message,
//         error: {}
//     });
// });

module.exports = app;