const mongoose = require('mongoose');
var usersSchema = mongoose.Schema({
    first_name: {
        type: String,
        required: true
    },
    last_name: {
        type: String,
        required: true
    },
    email: {
        type: String,
        required: true
    },
    password: {
        type: String,
        required: true
    },
    date_created: {
        type: Date,
        default: Date.now()
    }
});

var Users = module.exports = mongoose.model('Users', usersSchema);

module.exports.getUserInfo = function(callback, limit){
    Users.find(callback).limit(limit);
}