const mongoose = require('mongoose');
var productsSchema = mongoose.Schema({
    product_name: {
        type: String,
        required: true
    },
    date_created: {
        type: Date,
        default: Date.now()
    }
});

var Products = module.exports = mongoose.model('Products', productsSchema);

module.exports.getProductInfo = function(callback, limit){
    Products.find(callback).limit(limit);
}